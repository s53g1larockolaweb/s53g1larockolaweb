from flask import Flask, request
import pymysql
app = Flask(__name__)

def servidor():
    #'''
    HOST = "db4free.net"
    USER = "rockolawebs53"
    PSW = "mintic2021"
    #'''
    '''
    HOST = "127.0.0.1"
    USER = "root" 
    PSW = "bases"
    '''

    PORT = 3306
    DB = "canciones_rock53"
    
    return pymysql.connect(host = HOST, port = PORT, user = USER, password = PSW, db = DB)

@app.route("/")
def inicio():
    return '''
        <header><title>Inicio - RockolaWeb.com</title></header>
        <hr>

        <h1>¡Bienvenido a nuestra Rockola Virtual!</h1>
        <hr>

        <p>
            Por ahora tenemos pocas canciones, pero de seguro no te decepcionarán... <br><br>
            Puedes ver el listado completo de nuestras canciones presionando en: "Ver Listado Completo"... <br><br>
            También puedes intentar ver si alguna de tus canciones favoritas esta en nuestro
            listado de canciones presionando en: "Filtrar Búsqueda."
        </p>
        <hr>

        <ul>
            <li><a href="/listado">Ver Listado Completo</a></li>
            <li><a href="/filtro">Filtrar Búsqueda</a></li>
        </ul>
        <hr>
    '''

@app.route("/filtro")
def filtro():
    return '''
    <header><title>Filtro - RockolaWeb.com</title></head>
    <hr>

    <h1>Búsqueda Personalizada</h1>
    <hr>

    <h3>¡Atención!</h3>
    <p>
        No es necesario llenar todos los campos para realizar la búsqueda... <br>
        Entre más campos llene más específica será la búsqueda.
    </p>
    <hr>

    <form action="listado" method="POST">
        <table>
            <tr>
                <td><label for="nombre">Nombre: </label></td>
                <td><input type="text" name="nombre"></td>
            </tr>

            <tr>
                <td><label for="artista">Artista: </label></td>
                <td><input type="text" name="artista"></td>
            </tr>

            <tr>
                <td><label for="genero">Género: </label></td>
                <td><input type="text" name="genero"></td>
            </tr>

            <tr>
                <td><label for="anio">Año: </label></td>
                <td><input type="number" name="anio"></td>
            </tr>
        </table>
        <br>

        <input type="submit" value=" Buscar Canción ">
        <input type="reset" value=" Limpiar Campos ">
        <br><br><hr>

        <ul>
            <li><a href="/listado">Ver Listado Completo</a></li>
            <li><a href="/">Regresar a la página principal</a></li>
        </ul>
        <hr>
    </form>
    '''

@app.route("/listado", methods = ["GET", "POST"])
def listado():

    sql = "SELECT * FROM canciones_rock53.canciones"
    if request.method == "POST":
        nombre = request.form["nombre"]
        artista = request.form["artista"]
        genero = request.form["genero"]
        anio = request.form["anio"]

        if(nombre or artista or genero or anio): sql += " WHERE "

        if(nombre): sql += f"nombre LIKE '%{nombre}%' AND "
        if(artista): sql += f"artista LIKE '%{artista}%' AND "
        if(genero): sql += f"genero LIKE '%{genero}%' AND "
        if(anio): sql += f"anio LIKE '%{anio}%' AND "

        if(nombre or artista or genero or anio): sql += "TRUE"
    
    conexion = servidor()
    with conexion.cursor() as interprete:
        interprete.execute(sql)
        consulta = interprete.fetchall()
    conexion.close()
    
    html = f"""
    <header><title>Resultados - RockolaWeb.com</title></header>
    <hr>
    
    <h1>Resultados Encontrados ({len(consulta)})</h1>
    <hr>

    <ul>
    """

    if len(consulta) == 0:
        html += """
        </ul>

        <p>
            Lo sentimos, esta canción aún no se encuentra en nuestra base de datos...<br>
            Es probable que pueda deberse a un problema de derechos de autor.
        </p>
        """
    else:
        for row in consulta:
            html += f'''
            <li>
                <a href="/reproductor/{row[1]}/{row[5].replace("/","")}">{row[1]} - {row[2]} ({row[3]} - {row[4]})</a>
            </li>
            '''
    
    html += '''
    </ul>
    <hr>

    <ul>
        <li><a href="/">Regresar a la página principal</a></li>
        <li><a href="/filtro">Filtrar Búsqueda</a></li>
    </ul>
    <hr>
    '''
    return html

@app.route("/reproductor/<nombre>/<link>")
def reproductor(nombre, link):
    link = link.replace("https:", "https://")
    link = link.replace(".com", ".com/")
    link = link.replace("embed", "embed/")
    
    return f'''
    <header><title>Música - RockolaWeb.com</title></head>
    <hr>

    <h1>{nombre}</h1>
    <hr>
    
    <iframe width="560" height="315" src="{link}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <hr>

    <ul>
        <li><a href="/">Regresar a la página principal</a></li>
        <li><a href="/filtro">Filtrar Búsqueda</a></li>
    </ul>
    <hr>
    '''

def actServidor(sql):
    conexion = servidor()
    with conexion.cursor() as interprete:
        interprete.execute(sql)
        conexion.commit()
    conexion.close()

@app.route("/admin")
@app.route("/admin/<act>", methods = ["GET", "POST"])
def modificar(act = "norm"):
    mensaje = '<font color="#FF0000">'

    if request.method == "POST":
        if act == "del":
            mensaje += "(Canción eliminada exitosamente)"
            id = request.form["id"]
            sql = f"DELETE FROM canciones_rock53.canciones WHERE id={id}"
            actServidor(sql)

        elif act == "add" or act == "act":
            nombre = request.form["nombre"]
            artista = request.form["artista"]
            genero = request.form["genero"]
            anio =  request.form["anio"]
            link =  request.form["link"]

            if nombre and artista and genero and anio and link:
                mensaje += "(Canción "

                if act == "add":
                    sql = f"INSERT INTO canciones_rock53.canciones (nombre, artista, genero, anio, link) VALUES ('{nombre}', '{artista}', '{genero}', {anio}, '{link}')"
                    mensaje += "guardada"
                    
                elif act == "act":
                    id = request.form["id"]
                    sql = f"UPDATE canciones_rock53.canciones SET nombre='{nombre}', artista='{artista}', genero='{genero}', anio={anio}, link='{link}' WHERE id={id}"
                    mensaje += "actualizada"

                mensaje += " exitosamente)"
                actServidor(sql)
            
            else:
                mensaje += "(No se "

                if act == "add":
                    mensaje += "guardó"
                    
                elif act == "act":
                    mensaje += "actualizó"

                mensaje += " la canción, recuerde llenar todos los campos)"

    mensaje += "</font>"

    sql = "SELECT * FROM canciones_rock53.canciones"
    conexion = servidor()
    with conexion.cursor() as interprete:
        interprete.execute(sql)
        consulta = interprete.fetchall()
    conexion.close()

    html = f'''
    <header><title>Administrar Canciones - RockolaWeb.com</title></head>
    <hr>

    <h1>Expanda y modifique su repertorio musical con nuevas canciones</h1>
    <hr>

    <h3>¡Atención! {mensaje}</h3>
    <p>
        Es necesario llenar todos los campos para registrar una nueva canción...<br>
        Y también para modificar una canción ya existente en la base de datos.
    </p>
    <hr>
    <table>
            <tr>
                <th>Nombre</th>
                <th>Artista</th>
                <th>Género</th>
                <th>Año</th>
                <th>Link</th>
                <th>Opciones </th>
                <th></th>
            </tr>

            <tr>
            <form action="/admin/add" method="POST">
                <td><input type="text" size="20" name="nombre"></td>
                <td><input type="text" size="20" name="artista"></td>
                <td><input type="text" size="10" name="genero"></td>
                <td><input type="number" min="0" max="2021" name="anio"></td>
                <td><input type="text" size="45" name="link"></td>
                <td><input type="submit" value=" Agregar  "></td>
            </form>
        </tr>
    '''

    for row in consulta:
        html += f'''
        <tr>
            <form action="/admin/act" method="POST">
                <td><input type="text" size="20" name="nombre" value="{row[1]}"></td>
                <td><input type="text" size="20" name="artista" value="{row[2]}"></td>
                <td><input type="text" size="10" name="genero" value="{row[3]}"></td>
                <td><input type="number" min="0" max="2021" name="anio" value="{row[4]}"></td>
                <td><input type="text" size="45" name="link" value="{row[5]}"></td>

                <input type="hidden" name="id" value="{row[0]}">
                <td><input type="submit" value="Actualizar"></td>
            </form>

            <form action="/admin/del" method="POST">
                <input type="hidden" name="id" value="{row[0]}">
                <td><input type="submit" value=" Eliminar "></td>
            </form>
        </tr>
        '''
    html += """
    </table><br><hr>
    <ul><li><a href="/">Regresar a la página principal</a></li></ul><hr>
    """
    return html

if __name__ == "__main__":
    app.run( debug = True )