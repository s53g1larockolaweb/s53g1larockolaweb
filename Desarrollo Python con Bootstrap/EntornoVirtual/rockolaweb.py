# Capa Logica y Estructural del Proyecto

from flask import Flask, request, render_template, url_for
from interprete import modifyServidor, consultServidor
app = Flask(__name__)

@app.route("/")
def inicio(): return render_template("inicio.html")

@app.route("/filtro")
def filtro(): return render_template("filtro.html")

@app.route("/resultados", methods = ["GET", "POST"])
def resultados():

    sql = "SELECT * FROM canciones_rock53.canciones"
    if request.method == "POST":
        nombre = request.form["nombre"]
        artista = request.form["artista"]
        genero = request.form["genero"]
        anio = request.form["anio"]

        if(nombre or artista or genero or anio): sql += " WHERE "

        if(nombre): sql += f"nombre LIKE '%{nombre}%' AND "
        if(artista): sql += f"artista LIKE '%{artista}%' AND "
        if(genero): sql += f"genero LIKE '%{genero}%' AND "
        if(anio): sql += f"anio LIKE '%{anio}%' AND "

        if(nombre or artista or genero or anio): sql += "TRUE"
    
    consulta = consultServidor(sql)
    return render_template("resultados.html", resultados=len(consulta), consulta=consulta)

@app.route("/reproductor/<nombre>/<artista>/<genero>/<anio>/<link>")
def reproductor(nombre, artista, genero, anio, link):
    link = link.replace("https:", "https://")
    link = link.replace(".com", ".com/")
    link = link.replace("embed", "embed/")
    return render_template("reproductor.html", nombre=nombre, artista=artista, genero=genero, anio=anio, link=link)

@app.route("/admin")
@app.route("/admin/<act>", methods = ["GET", "POST"])
def modificar(act = "norm"):

    color = "#109748" # VERDE - BIEN
    if act == "norm": color = "#FFFFFF" # BLANCO - NEUTRAL
    mensaje = ""
    
    if request.method == "POST":
        if act == "del":
            mensaje += "(Canción eliminada exitosamente)"
            id = request.form["id"]
            sql = f"DELETE FROM canciones_rock53.canciones WHERE id={id}"
            modifyServidor(sql)

        elif act == "add" or act == "act":
            nombre = request.form["nombre"]
            artista = request.form["artista"]
            genero = request.form["genero"]
            anio =  request.form["anio"]
            link =  request.form["link"]

            if nombre and artista and genero and anio and link:
                mensaje += "(Canción "

                if act == "add":
                    sql = f"INSERT INTO canciones_rock53.canciones (nombre, artista, genero, anio, link) VALUES ('{nombre}', '{artista}', '{genero}', {anio}, '{link}')"
                    mensaje += "guardada"
                    
                elif act == "act":
                    id = request.form["id"]
                    sql = f"UPDATE canciones_rock53.canciones SET nombre='{nombre}', artista='{artista}', genero='{genero}', anio={anio}, link='{link}' WHERE id={id}"
                    mensaje += "actualizada"

                mensaje += " exitosamente)"
                modifyServidor(sql)

            else:
                color = "#CF3944" # ROJO - MAL
                mensaje += "(No se "
                
                if act == "add": mensaje += "guardó"
                elif act == "act": mensaje += "actualizó"

                mensaje += " la canción, recuerde llenar todos los campos)"
    
    sql = "SELECT * FROM canciones_rock53.canciones"
    consulta = consultServidor(sql)
    return render_template("admin.html", color=color, mensaje=mensaje, consulta=consulta)

if __name__ == "__main__":
    app.run( debug = True )