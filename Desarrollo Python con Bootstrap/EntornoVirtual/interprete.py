# Capa de Persistencia del Proyecto
import pymysql

# Conexion con el servidor
def servidor():
    #'''
    HOST = "db4free.net"
    USER = "rockolawebs53"
    PSW = "mintic2021"
    #'''
    '''
    HOST = "127.0.0.1"
    USER = "root" 
    PSW = "bases"
    '''

    PORT = 3306
    DB = "canciones_rock53"
    
    return pymysql.connect(host = HOST, port = PORT, user = USER, password = PSW, db = DB)

# Modificacion (Actualizar o Eliminar) del servidor
def modifyServidor(sql):
    conexion = servidor()
    with conexion.cursor() as interprete:
        interprete.execute(sql)
        conexion.commit()
    conexion.close()

# Consulta al servidor
def consultServidor(sql):
    conexion = servidor()
    with conexion.cursor() as interprete:
        interprete.execute(sql)
        consulta = interprete.fetchall()
    conexion.close()
    return consulta
