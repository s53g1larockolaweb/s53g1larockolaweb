# Resumen del Proyecto - RockolaWeb
RockolaWeb es un proyecto de aplicación web diseñado para facilitar la gestión y reproducción de canciones en línea. Desarrollado con Python y Flask, esta aplicación te permite agregar, eliminar y modificar canciones directamente desde tu navegador web. Utilizamos tecnologías como Python y Flask para el backend, junto con templates de Flask para el frontend, asegurando una experiencia de usuario eficiente y amigable.

# Características principales:
* **Operaciones CRUD:** Agregar, eliminar y modificar canciones directamente desde la interfaz web.
* **Reproductor Multimedia:** Visualización de canciones en un reproductor multimedia integrado.
* **Filtrado Avanzado:** Funcionalidad para filtrar canciones por género, año y otros criterios.
* **Metodologías de Desarrollo:** Implementación de prácticas ágiles con Jira para la gestión de tareas y reuniones diarias (dailies).
* **Tecnologías Utilizadas:** Python, Flask, MySQL, Git.

Este repositorio alberga el código fuente del proyecto RockolaWeb, incluyendo el backend desarrollado en Python con Flask, el frontend utilizando templates de Flask, y la integración con una base de datos MySQL para almacenar y gestionar la información de las canciones.