package Logica;
import java.util.Map;
public class CtrlCanciones
{
    MdlCanciones modelo = new MdlCanciones();

    public Map<Integer,TOCanciones> listarPersonas(){
        return modelo.listarCanciones();
    }

    public TOCanciones consultarCancionXNombre(String nombre){
        return modelo.consultarCancionXnombre(nombre);
    }

    public boolean insertarCancion(TOCanciones cancion){
        return modelo.insertarCancion(cancion);
    }

    public boolean actualizarCancion(TOCanciones cancion){
        return modelo.actualizarCancion(cancion);
    }

    public boolean elimiarCancion(String nombre){
        return modelo.eliminarCancion(nombre);
    }
}