package Logica;

//import db.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.HashMap;

public class MdlCanciones {
    //private ConexionBD con = new ConexionBD();

    public Map<Integer,TOCanciones> listarCanciones(){
        Map<Integer,TOCanciones> canciones = new HashMap<Integer,TOCanciones>();
        TOCanciones cancion;
        /*try{
            ResultSet rs= con.consultarTabla("Personas");

            while(rs.next()){
                cancion = new TOCanciones(rs.getString("nombre"),rs.getString("artista"),rs.getString("genero"),rs.getInt("ano"),rs.getString("link"));
                canciones.put(rs.getInt("idCanciones"),cancion);
            }
        }
        catch(SQLException e){
            System.out.println("ErrorMdlCanciones.listarCanciones"+e.getMessage());

        }*/
        return canciones;
    }

    public TOCanciones consultarCancionXnombre(String nombre){
        TOCanciones cancion;
        /*try{
            ResultSet rs= con.consultarTablaWhere("Canciones", " nombre = '"+nombre+"'");

            while(rs.next()){
                cancion = new TOCanciones(rs.getString("nombre"),rs.getString("artista"),rs.getString("genero"),rs.getInt("ano"),rs.getString("link"));
                cancion.setId(rs.getInt("idCanciones"));
                return cancion;
            }
        }catch(SQLException e){
            System.out.println("ErrorMdlCanciones.consultarCancionXNombre"+e.getMessage());
            return null;
        }*/
        return null;
    }

    public boolean insertarCancion(TOCanciones cancion){
        String[] campos = {"nombre" ,"artista" ,"genero","ano","link"};
        String[] valores = {cancion.getNombre(),cancion.getArtista(),cancion.getGenero(),cancion.getAno(),cancion.getLink()};
        /*try{
            return con.insertar("Canciones", campos, valores);
        }catch(Exception e){
            System.out.println("ErrorMdlCanciones.insertarCancion"+e.getMessage());*/
            return false;
        }

    public boolean actualizarCancion(TOCanciones cancion){
        String[] campos = {"nombre" ,"artista" ,"genero","ano","link"};
        String[] valores = {cancion.getNombre(),cancion.getArtista(),cancion.getGenero(),cancion.getAno(),cancion.getLink()};
        /*try{
            return con.actualizar("Canciones", campos, valores, cancion.getId());
        }catch(Exception e){
            System.out.println("ErrorMdlCanciones.actualizarCancion"+e.getMessage());*/
            return false;
        }


    public boolean eliminarCancion(String nombre){
        return false;
    }}
        /*try{
            return con.eliminar("Canciones", nombre);
        }catch(Exception e){
            System.out.println("ErrorMdlCanciones.eliminarCancion"+e.getMessage());
            return false;
        }
    }*/