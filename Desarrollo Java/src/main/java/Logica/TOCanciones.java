package Logica;

public class TOCanciones {
    private int id;
    private String nombre;
    private String artista;
    private String genero;
    private String ano;
    private String link;

    public TOCanciones(String nombre, String artista, String genero, String ano, String link) {
        this.nombre = nombre;
        this.artista = artista;
        this.genero = genero;
        this.ano = ano;
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "TOCanciones{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", artista='" + artista + '\'' +
                ", genero='" + genero + '\'' +
                ", ano=" + ano +
                ", link='" + link + '\'' +
                '}';
    }
}
